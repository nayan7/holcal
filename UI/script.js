$(document).ready(function() {
  $('#calendar').evoCalendar({

    theme: 'Midnight Blue',

    calendarEvents: [
      {
        id: '1', // Event's ID (required)
        name: "New Year", // Event name (required)
        date: "January/1/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: true // Same event every year (optional)
      },
      {
        id:'2',
        name: "Vacation Leave",
        badge: "02/13 - 02/15", // Event badge (optional)
        date: ["February/13/2021", "February/15/2021"], // Date range
        description: "Vacation leave for 3 days.", // Event description (optional)
        type: "event",
        color: "#63d867" // Event custom color (optional)
      },
      {
        id: '3', // Event's ID (required)
        name: "Makara Sankranti", // Event name (required)
        date: "January/14/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '4', // Event's ID (required)
        name: "Republic Day", // Event name (required)
        date: "January/26/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: true // Same event every year (optional)
      },
      {
        id: '5', // Event's ID (required)
        name: "Wellness Day", // Event name (required)
        date: "March/19/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '6', // Event's ID (required)
        name: "Good Friday", // Event name (required)
        date: "April/02/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '7', // Event's ID (required)
        name: "Chandramana Ugadi", // Event name (required)
        date: "April/13/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '8', // Event's ID (required)
        name: "May Day", // Event name (required)
        date: "May/01/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '9', // Event's ID (required)
        name: "Ramzan/Id-ul Fitr", // Event name (required)
        date: "May/14/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '10', // Event's ID (required)
        name: "Wellness Day", // Event name (required)
        date: "May/28/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '11', // Event's ID (required)
        name: "Wellness Day", // Event name (required)
        date: "July/30/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '12', // Event's ID (required)
        name: "Independence Day", // Event name (required)
        date: "August/15/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false// Same event every year (optional)
      },
      {
        id: '13', // Event's ID (required)
        name: "Wellness Day", // Event name (required)
        date: "September/03/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '14', // Event's ID (required)
        name: "Ganesh Chaturthi/Vinayakar Chaturthi", // Event name (required)
        date: "September/10/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '15', // Event's ID (required)
        name: "Gandhi Jayanti", // Event name (required)
        date: "October/2/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: true // Same event every year (optional)
      },
      {
        id: '16', // Event's ID (required)
        name: "Vijayadasami", // Event name (required)
        date: "October/15/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '17', // Event's ID (required)
        name: "Rajyotsava Day", // Event name (required)
        date: "November/01/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      },
      {
        id: '18', // Event's ID (required)
        name: "Balipadyami", // Event name (required)
        date: "November/18/2021", // Event date (required)
        type: "holiday", // Event type (required)
        everyYear: false // Same event every year (optional)
      }
    ]      
  })
})
